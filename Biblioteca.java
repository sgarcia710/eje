public class Biblioteca {
//Atributos:
	private int numPisos;
	private String direccion;
	private int estantes;
	private int ejemplares;
	
	
	public Biblioteca(int numPisos, String direccion, int estantes, int ejemplares) {
		super();
		this.numPisos = numPisos;
		this.direccion = direccion;
		this.estantes = estantes;
		this.ejemplares = ejemplares;
	}
}
